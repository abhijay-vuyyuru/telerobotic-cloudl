#include <stdio.h>
#include <curl/curl.h>
#include <iostream>
#include <cstring>
#include "Leap.h"
#include <fstream>
#include<string>

using namespace Leap;

class SampleListener : public Listener {
  public:
    virtual void onInit(const Controller&);
    virtual void onConnect(const Controller&);
    virtual void onDisconnect(const Controller&);
    virtual void onExit(const Controller&);
    virtual void onFrame(const Controller&);
    virtual void onFocusGained(const Controller&);
    virtual void onFocusLost(const Controller&);
    virtual void onDeviceChange(const Controller&);
    virtual void onServiceConnect(const Controller&);
    virtual void onServiceDisconnect(const Controller&);

  private:
};

const std::string fingerNames[] = {"Thumb", "Index", "Middle", "Ring", "Pinky"};
const std::string boneNames[] = {"Metacarpal", "Proximal", "Middle", "Distal"};
const std::string stateNames[] = {"STATE_INVALID", "STATE_START", "STATE_UPDATE", "STATE_END"};
int count_key_tap = 0;
int count_screen_tap = 0 ;
int count_swipe = 0;
int count_circle_clockwise = 0;
int count_circle_anticlockwise = 0;

void SampleListener::onInit(const Controller& controller) {
  std::cout << "Initialized" << std::endl;
}

void SampleListener::onConnect(const Controller& controller) {
  std::cout << "Connected" << std::endl;
  controller.enableGesture(Gesture::TYPE_CIRCLE);
  controller.enableGesture(Gesture::TYPE_KEY_TAP);
  controller.enableGesture(Gesture::TYPE_SCREEN_TAP);
  controller.enableGesture(Gesture::TYPE_SWIPE);
}

void SampleListener::onDisconnect(const Controller& controller) {
  // Note: not dispatched when running in a debugger.
  std::cout << "Disconnected" << std::endl;
}

void SampleListener::onExit(const Controller& controller) {
  std::cout << "Exited" << std::endl;
}

void SampleListener::onFrame(const Controller& controller) {
  // Get the most recent frame and report some basic information
  const Frame frame = controller.frame();
  /*std::cout << "Frame id: " << frame.id()
            << ", timestamp: " << frame.timestamp()
            << ", hands: " << frame.hands().count()
            << ", extended fingers: " << frame.fingers().extended().count()
            << ", tools: " << frame.tools().count()
            << ", gestures: " << frame.gestures().count() << std::endl;*/

  HandList hands = frame.hands();
  for (HandList::const_iterator hl = hands.begin(); hl != hands.end(); ++hl) {
    // Get the first hand
    const Hand hand = *hl;
    std::string handType = hand.isLeft() ? "Left hand" : "Right hand";
    /*std::cout << std::string(2, ' ') << handType << ", id: " << hand.id()
              << ", palm position: " << hand.palmPosition() << std::endl;*/
    // Get the hand's normal vector and direction
    const Vector normal = hand.palmNormal();
    const Vector direction = hand.direction();

    // Calculate the hand's pitch, roll, and yaw angles
/*    std::cout << std::string(2, ' ') <<  "pitch: " << direction.pitch() * RAD_TO_DEG << " degrees, "
              << "roll: " << normal.roll() * RAD_TO_DEG << " degrees, "
              << "yaw: " << direction.yaw() * RAD_TO_DEG << " degrees" << std::endl;*/

    // Get the Arm bone
    Arm arm = hand.arm();
   /* std::cout << std::string(2, ' ') <<  "Arm direction: " << arm.direction()
              << " wrist position: " << arm.wristPosition()
              << " elbow position: " << arm.elbowPosition() << std::endl;*/

    // Get fingers
    const FingerList fingers = hand.fingers();
    for (FingerList::const_iterator fl = fingers.begin(); fl != fingers.end(); ++fl) {
      const Finger finger = *fl;
      /*std::cout << std::string(4, ' ') <<  fingerNames[finger.type()]
                << " finger, id: " << finger.id()
                << ", length: " << finger.length()
                << "mm, width: " << finger.width() << std::endl;*/

      // Get finger bones
      for (int b = 0; b < 4; ++b) {
        Bone::Type boneType = static_cast<Bone::Type>(b);
        Bone bone = finger.bone(boneType);
       /* std::cout << std::string(6, ' ') <<  boneNames[boneType]
                  << " bone, start: " << bone.prevJoint()
                  << ", end: " << bone.nextJoint()
                  << ", direction: " << bone.direction() << std::endl;*/
      }
    }
  }

  // Get tools
  const ToolList tools = frame.tools();
  for (ToolList::const_iterator tl = tools.begin(); tl != tools.end(); ++tl) {
    const Tool tool = *tl;
/*    std::cout << std::string(2, ' ') <<  "Tool, id: " << tool.id()
              << ", position: " << tool.tipPosition()
              << ", direction: " << tool.direction() << std::endl;*/
  }

  // Get gestures
  
  const GestureList gestures = frame.gestures(); // Get gestures in the frame
  for (int g = 0; g < gestures.count(); ++g) {
    Gesture gesture = gestures[g];

    switch (gesture.type()) {
      case Gesture::TYPE_CIRCLE:
      {
        CircleGesture circle = gesture;
        std::string clockwiseness;

        if (circle.pointable().direction().angleTo(circle.normal()) <= PI/2) {

          clockwiseness = "clockwise";
	  count_screen_tap = 0; //Reset counters for other gestures
	  count_key_tap = 0;
	  count_swipe = 0;
	  count_circle_anticlockwise = 0;
	
	  

	if(stateNames[gesture.state()]=="STATE_END")
{		
	std::cout << "State End found" << std::endl;
	count_circle_clockwise+=1;
	
}
	
	if(count_circle_clockwise >=2)
	{	std::cout << "Clockwise circle detected.. " << std::endl;
		count_circle_clockwise = 0; 
		std::cout << "Compliling.." << std::endl;
	        system("g++ -std=c++11 vision.cpp -o vision `pkg-config --cflags --libs opencv`");
	        std::cout << "Live stream started..Getting coordinates" << std::endl;
	
	        system("./vision");
		count_screen_tap = 0; //Reset the counter for the original gesture.


		std::ifstream in("out.txt");// Read the coordinates to move robot to.
		double num1,num2;
		in >> num1 >> num2;
		std::cout<< "Num1 is" << num1;
		std::cout<< "Num2 is" << num2;
		std::string url;
		std::string x_string = std::to_string(num1);
		std::string y_string = std::to_string(num2+20); //Error correction
		std::string firstpart("http://10.234.2.220:8081/mywebservice/WebService1.asmx/ChnageOffsetsXYZ?controllerIPAddress=10.234.3.101&x=");
		std::string secondpart("&y=");
		std::string thirdpart("&z=-287.52");
		url = firstpart + x_string + secondpart + y_string + thirdpart; // Create the webservice URL
		in.close();
		//std::cout << url.c_str() << '\n';
	
		CURL *curl;
		CURL *curl1;

 		 CURLcode res;
		CURLcode res1;
 
  curl = curl_easy_init();
  curl1 = curl_easy_init();

  if(curl) {
   // curl_easy_setopt(curl1, CURLOPT_URL, "http://10.234.2.220:8081/mywebservice/WebService1.asmx/ChnageOffsetsXYZ?controllerIPAddress=10.234.3.101&x=27.438000&y=206.898000&z=-337.52");
curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
std::cout << "URL called is " <<  url.c_str() << std::endl;


//curl_easy_setopt(curl1, CURLOPT_URL, "http://10.234.2.220:8081/mywebservice/WebService1.asmx/SetFlags?controllerIPAddress=10.234.3.101&flagName=flag_OffsetMove&value=true");

    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
 

    res = curl_easy_perform(curl);

    if(res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
 

    curl_easy_cleanup(curl);
  }
if(curl1) {
    //curl_easy_setopt(curl, CURLOPT_URL, "http://10.234.2.220:8081/mywebservice/WebService1.asmx/ChnageOffsetsXYZ?controllerIPAddress=10.234.3.101&x=27.438000&y=206.898000&z=-337.52");
//curl_easy_setopt(curl1, CURLOPT_URL, url2.c_str());
//std::cout << "URL is " <<  url2.c_str() << std::endl;
curl_easy_setopt(curl1, CURLOPT_URL, "http://10.234.2.220:8081/mywebservice/WebService1.asmx/SetFlags?controllerIPAddress=10.234.3.101&flagName=flag_OffsetMove&value=true");

    curl_easy_setopt(curl1, CURLOPT_FOLLOWLOCATION, 1L);
 

    res1 = curl_easy_perform(curl1);

    if(res1 != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res1));
 

    curl_easy_cleanup(curl1);
  }

		
	}
	else
		std::cout << "Expecting more screen taps. Current is " <<count_screen_tap<< std::endl;
	

	


	
	//std::cout << "Compliling.." << std::endl;
	//system("g++ -std=c++11 vision.cpp -o vision `pkg-config --cflags --libs opencv`");
	//std::cout << "Live stream started..Getting coordinates" << std::endl;
	
	//system("./vision");
		
	
        } 
	else {
          clockwiseness = "counterclockwise";
	  count_screen_tap = 0;
	  count_key_tap = 0;
	  count_swipe = 0;
	  count_circle_clockwise = 0;

	 if(stateNames[gesture.state()]=="STATE_END")
{		
	std::cout << "State End found" << std::endl;
	count_circle_anticlockwise+=1;
	
}
	
	if(count_circle_anticlockwise >= 2)
	{	std::cout << "AntiClockwise circle detected.. Closing Gripper.." << std::endl;
		count_circle_anticlockwise = 0; 
		 CURL *curl;
  CURLcode res;
 
  curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "http://10.234.2.220:8081/mywebservice/WebService1.asmx/SetFlags?controllerIPAddress=10.234.3.101&flagName=flag_GripperClose&value=true");

    
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
 
    
    res = curl_easy_perform(curl);
     
    if(res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
 
     
    curl_easy_cleanup(curl);
  }


		

}
	else
		std::cout << "Expecting more state ends. Current is " << count_circle_anticlockwise << std::endl;

	
        }

        // Calculate angle swept since last frame
        float sweptAngle = 0;
        if (circle.state() != Gesture::STATE_START) {
          CircleGesture previousUpdate = CircleGesture(controller.frame(1).gesture(circle.id()));
          sweptAngle = (circle.progress() - previousUpdate.progress()) * 2 * PI;
        }
        std::cout << std::string(2, ' ')
                  << "Circle id: " << gesture.id()
                  << ", state: " << stateNames[gesture.state()]
                  << ", progress: " << circle.progress()
                  << ", radius: " << circle.radius()
                  << ", angle " << sweptAngle * RAD_TO_DEG
                  <<  ", " << clockwiseness << std::endl;
        break;
      }
      case Gesture::TYPE_SWIPE:
      {
	count_screen_tap = 0;
	count_key_tap = 0;
	count_circle_clockwise = 0;
	count_circle_anticlockwise = 0;
	
	
        SwipeGesture swipe = gesture;
        std::cout << std::string(2, ' ')
          << "Swipe id: " << gesture.id()
          << ", state: " << stateNames[gesture.state()]
          << ", direction: " << swipe.direction()
          << ", speed: " << swipe.speed() << std::endl;
	if(stateNames[gesture.state()]=="STATE_END")
{		
	std::cout << "State End found" << std::endl;
	count_swipe+=1;
	
}
	
	if(count_swipe >= 2)
	{	std::cout << "Swipe Detected. Calling web service to move to base position..." << std::endl;
		count_swipe = 0;
		
		
			 CURL *curl;
 	 CURLcode res;
 
  curl = curl_easy_init();
  if(curl) {
    //curl_easy_setopt(curl, CURLOPT_URL, "http://10.234.2.220:8081/mywebservice/WebService1.asmx/SetFlags?controllerIPAddress=10.234.3.101&flagName=flag_GripperOpen&value=true");
curl_easy_setopt(curl, CURLOPT_URL, "http://10.234.2.220:8081/mywebservice/WebService1.asmx/SetFlags?controllerIPAddress=10.234.3.101&flagName=flag_GOTO_T40&value=true");
   
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
 
   
    res = curl_easy_perform(curl);
    
    if(res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
 
    
    curl_easy_cleanup(curl);
  }



	}
	else
		std::cout << "Expecting more swipes. Current is " << count_swipe << std::endl;





        break;
      }
      case Gesture::TYPE_KEY_TAP:
      {
	count_key_tap+=1;
	count_screen_tap = 0;
	count_swipe = 0;
	count_circle_clockwise = 0;
	count_circle_anticlockwise = 0;
	    
	
        KeyTapGesture tap = gesture;
        std::cout << std::string(2, ' ')
          << "Key Tap id: " << gesture.id()
          << ", state: " << stateNames[gesture.state()]
          << ", position: " << tap.position()
          << ", direction: " << tap.direction()<< std::endl;
	if(count_key_tap >= 2)
	{	std::cout << "Key Tap detected. Calling web service to lower gripper to pick up object..." << std::endl;
		count_key_tap = 0;
			

		std::ifstream in2("out.txt");
		double num12,num22;
		in2 >> num12 >> num22;
		std::cout<< "Num1 is" << num12;
		std::cout<< "Num2 is" << num22;
		std::string url2;
		std::string x_string2 = std::to_string(num12);
		std::string y_string2 = std::to_string(num22+10);
		std::string firstpart2("http://10.234.2.220:8081/mywebservice/WebService1.asmx/ChnageOffsetsXYZ?controllerIPAddress=10.234.3.101&x=");
		std::string secondpart2("&y=");
		std::string thirdpart2("&z=-337.52");
		url2 = firstpart2 + x_string2 + secondpart2 + y_string2 + thirdpart2;
		in2.close();
		std::cout << "URL2 is " <<  url2.c_str() << std::endl;
		
		
		

			 CURL *curl;
			 CURL *curl1;
  		CURLcode res;
 		CURLcode res1;
		  curl = curl_easy_init();
		  curl1 = curl_easy_init();

  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, url2.c_str());
    std::cout << "URL is " <<  url2.c_str() << std::endl;
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    res = curl_easy_perform(curl);

    if(res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",curl_easy_strerror(res));

    curl_easy_cleanup(curl);
  }
if(curl1) {

curl_easy_setopt(curl1, CURLOPT_URL, "http://10.234.2.220:8081/mywebservice/WebService1.asmx/SetFlags?controllerIPAddress=10.234.3.101&flagName=flag_OffsetMove&value=true");

    curl_easy_setopt(curl1, CURLOPT_FOLLOWLOCATION, 1L);
 

    res1 = curl_easy_perform(curl1);

    if(res1 != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res1));
 

    curl_easy_cleanup(curl1);
  } 
					
	}
	else
		std::cout << "Expecting more key taps. Current is " << count_key_tap << std::endl;
     
        break;
      }
      case Gesture::TYPE_SCREEN_TAP:
      {
	count_screen_tap += 1;
	count_key_tap=0;
	count_swipe = 0;
	count_circle_clockwise = 0;
	count_circle_anticlockwise = 0;
    
        ScreenTapGesture screentap = gesture;
        std::cout << std::string(2, ' ')
          << "Screen Tap id: " << gesture.id()
          << ", state: " << stateNames[gesture.state()]
          << ", position: " << screentap.position()
          << ", direction: " << screentap.direction()<< std::endl;
	if(count_screen_tap >= 2)
	{	


		std::cout << "Screen Tap detected. Opening gripper.." << std::endl;

			 CURL *curl;
  CURLcode res;
 
  curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "http://10.234.2.220:8081/mywebservice/WebService1.asmx/SetFlags?controllerIPAddress=10.234.3.101&flagName=flag_GripperOpen&value=true");

    
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
 
    
    res = curl_easy_perform(curl);
     
    if(res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
 
     
    curl_easy_cleanup(curl);
  }
			
}
	else
		std::cout << "Expecting more screen tap. Current is " << count_screen_tap << std::endl;
	
        break;
      }
      default:
        std::cout << std::string(2, ' ')  << "Unknown gesture type." << std::endl;
        break;
    }
  }

  if (!frame.hands().isEmpty() || !gestures.isEmpty()) {
    //std::cout << std::endl;
  }
}

void SampleListener::onFocusGained(const Controller& controller) {
  std::cout << "Focus Gained" << std::endl;
}

void SampleListener::onFocusLost(const Controller& controller) {
  std::cout << "Focus Lost" << std::endl;
}

void SampleListener::onDeviceChange(const Controller& controller) {
  std::cout << "Device Changed" << std::endl;
  const DeviceList devices = controller.devices();

  for (int i = 0; i < devices.count(); ++i) {
    std::cout << "id: " << devices[i].toString() << std::endl;
    std::cout << "  isStreaming: " << (devices[i].isStreaming() ? "true" : "false") << std::endl;
  }
}

void SampleListener::onServiceConnect(const Controller& controller) {
  std::cout << "Service Connected" << std::endl;
}

void SampleListener::onServiceDisconnect(const Controller& controller) {
  std::cout << "Service Disconnected" << std::endl;
}

int main(int argc, char** argv) {
  // Create a sample listener and controller
  SampleListener listener;
  Controller controller;

  // Have the sample listener receive events from the controller
  controller.addListener(listener);

  if (argc > 1 && strcmp(argv[1], "--bg") == 0)
    controller.setPolicy(Leap::Controller::POLICY_BACKGROUND_FRAMES);

  // Keep this process running until Enter is pressed
  std::cout << "Press Enter to quit..." << std::endl;
  std::cin.get();

  // Remove the sample listener when done
  controller.removeListener(listener);

  return 0;
}
