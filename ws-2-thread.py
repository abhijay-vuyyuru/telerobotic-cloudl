import time
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from SocketServer import ThreadingMixIn
import threading
import subprocess
import os
import sys
import webbrowser

#HOST_NAME = 'minnie'
#PORT_NUMBER = 9000
HOST_NAME = '10.234.2.225'
PORT_NUMBER = 8085

def run(cmd,file):
  #file.write('<br>Running '+str(cmd))
  p = subprocess.Popen(cmd, stdout=subprocess.PIPE, close_fds=True)
  for line in p.stdout:
    file.write('<br>'+line)
    file.flush()

# not very HTML-y
def runcat(cmd,file):
  p = subprocess.Popen(cmd, stdout=subprocess.PIPE, close_fds=True)
  for line in p.stdout:
    file.write(line)
    file.flush()

#import vnctracker

class MyHandler(BaseHTTPRequestHandler):
    def do_HEAD(s):
        s.send_response(200)

        s.send_header("Content-type", "text/html")
        s.end_headers()

    def do_PUT(s):
        """Respond to a PUT request."""
        s.send_header("Content-type", "text/html")
        s.end_headers()
        s.wfile.write("<html><head><title>VITELab Control Panel</title></head>")
        s.wfile.write("<p>You accessed path: %s</p>" % s.path)
        length = int(s.headers['Content-Length'])
        content = s.rfile.read(length)
        try:
          pass
        except Exception as e:
          print 'Exception '+str(e)
          s.wfile.write("<br>Exception "+str(e))
        s.send_response(200)

    def do_GET(s):
        """Respond to a GET request."""
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.end_headers()
        s.wfile.write("<html><head><title>VITELab Control Panel</title></head>")
        # If someone went to "http://something.somewhere.net/foo/bar/",
        # then s.path equals "/foo/bar/".

        message =  threading.currentThread().getName()
        s.wfile.write("<br>" +str(message))

        s.wfile.write("<p>You accessed path: %s</p>" % s.path)
        try:
          if s.path.startswith("/test0"):
            s.wfile.write("<br>Starting web service")
	    webbrowser.open('http://10.234.2.220:8081/mywebservice/WebService1.asmx/SetFlags?controllerIPAddress=10.234.3.101&flagName=flag_GripperClose&value=true')
	    
          elif s.path.startswith("/sage-start"):
            s.wfile.write("<br>Starting SAGE")      
            #s.wfile.write("<br>"+str(run(["/home/sage/googleearth-script/stop"],s.wfile)))
            #s.wfile.write("<br>"+str(run(["/home/sage/script/SAGE-stop"],s.wfile)))
            #s.wfile.write("<br>"+str(run(["/home/sage/script/SAGE-start"],s.wfile)))
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/VXLab-SAGE-start"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/sage-stop"):
            s.wfile.write("<br>Stopping SAGE")
            s.wfile.write("<br>"+str(run(["/home/sage/script/SAGE-stop"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/capture"):
            fullcmd=s.path.split("?")
            cmd=fullcmd[0]
            fields=cmd.split("-")
            s.wfile.write("<br>"+str(fields))
            input=fields[1]
            startstop=fields[2]
            if input in ['laptop1','laptop2','laptop3','vc1','vc2'] and startstop in ['start','stop']:
              shellcmd=["/home/sage/script/capturecard",input,startstop]
              s.wfile.write("<br>Cmd: "+str(shellcmd))
              run(shellcmd,s.wfile)
            else:
              s.wfile.write("<br>Unrecognized command")
            s.wfile.write("<br>done")
          elif s.path.startswith("/gearth-start"):
            s.wfile.write("<br>Starting Google Earth")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/VXLab-googleearth-start"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/stop-sage-gearth"):
            s.wfile.write("<br>Stopping both SAGE and Google Earth")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/VXLab-all-stop"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/toggle-SAGEPointer"):
            s.wfile.write("<br>Toggle SAGE Pointer")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/VXLab-SAGE-pointer-UserPointer"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/toggle-vncPointer"):
            s.wfile.write("<br>Toggle vnc Pointer")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/VXLab-vnc-pointer-UserPointer"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/sage-browser-amritlab-view-vc"):
            s.wfile.write("<br>Start SAGE browser amritlab view vc")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/sagebrowser-amritlab-vc"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/sage-browser-amritlab-view"):
            s.wfile.write("<br>Start SAGE browser amritlab view")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/sagebrowser-amritlab"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/sage-browser-4k"):
            s.wfile.write("<br>Start SAGE browser (hi-res)")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/sagebrowser-hires"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/amx-amritlab"):
            s.wfile.write("<br>Start AMRITLab AMX VNC Viewer")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/amx-amritlab"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/amx-vxlab"):
            s.wfile.write("<br>Start VXLab AMX VNC Viewer")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/amx-vxlab"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/sagebrowser-networkmonitor"):
            s.wfile.write("<br>Start VXLab Network Monitor")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/sagebrowser-networkmonitor"],s.wfile)))
            s.wfile.write("<br>done")

          elif s.path.startswith("/sage-browser-alice"):
            s.wfile.write("<br>Start COLENG alice profile")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/sagebrowser-coleng-alice"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/sage-browser-bob"):
            s.wfile.write("<br>Start COLENG bob profile")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/sagebrowser-coleng-bob"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/sage-browser-charles"):
            s.wfile.write("<br>Start COLENG charles profile")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/sagebrowser-coleng-charles"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/sage-browser-dora"):
            s.wfile.write("<br>Start COLENG dora profile")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/sagebrowser-coleng-dora"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/sage-browser-amritlabview-2"):
            s.wfile.write("<br>Start COLENG amritlab view")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/sagebrowser-coleng-amritlabview-2"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/sage-browser-amritlabview"):
            s.wfile.write("<br>Start COLENG amritlab view")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/sagebrowser-coleng-amritlabview"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/sage-browser-amprobot100"):
            s.wfile.write("<br>Start COLENG amp robot 100 profile")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/sagebrowser-coleng-amprobot100"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/sage-browser-eric"):
            s.wfile.write("<br>Start COLENG eric profile")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/sagebrowser-coleng-eric"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/sage-browser-fred"):
            s.wfile.write("<br>Start COLENG amp fred profile")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/sagebrowser-coleng-fred"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/sage-browser-url"):
            fullcmd=s.path.split("?")
            s.wfile.write("<br> param length: " + str(len(fullcmd)))
            s.wfile.write("<br> first: " + str(fullcmd[1]))
            siteurl= str(fullcmd[1])
#            siteurl="/usr/bin/nohup /home/sage/script/sagebrowser -geometry 1920x1080 -url '" + str(fullcmd[1])
            s.wfile.write("<br> org url: " + str(siteurl))
            if len(fullcmd) > 2:
             s.wfile.write("<br> > 2")
	     if str(fullcmd[2]):
              siteurl+= "?" + str(fullcmd[2])
              s.wfile.write("<br> second: " + str(fullcmd[2]))
              s.wfile.write("<br> concat")
            s.wfile.write("<br> full url: " + str(siteurl))
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/sagebrowser","-geometry","1920x1080","-url", str(siteurl)], s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/sage-browser"):
            s.wfile.write("<br>Start SAGE browser")
            s.wfile.write("<br>"+str(run(["/usr/bin/nohup","/home/sage/script/sagebrowser-lowres"],s.wfile)))
            s.wfile.write("<br>done")
          elif s.path.startswith("/get-vnc-urls"):
            vnctracker.getVncUrls(s)
          elif s.path.startswith("/set-vnc-url"):
            vnctracker.setVncUrl(s)
          elif s.path.startswith("/get-selected-images"):
            s.wfile.write("\nSTART-SELECTED-IMAGES\n")
            s.wfile.write(str(runcat(["/usr/bin/cat","/home/sage/sageMultiSelect.txt"],s.wfile)))
            s.wfile.write("\nEND-SELECTED-IMAGES\n")
            #s.wfile.write("<br>done")
          else:
            s.wfile.write("unrecognized command")
          s.wfile.write("</body></html>")
        except Exception as e:
          s.wfile.write("<br>Exception "+str(e))

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""

if __name__ == '__main__':
    print 'args '+str(len(sys.argv))
    args=len(sys.argv)
    if args<=1:
      print 'usage: ws-2-thread.py IPADDR'
      sys.exit()
    HOST_NAME = sys.argv[1]
    print 'HOST = '+str(HOST_NAME)
#   server_class = BaseHTTPServer.HTTPServer
#   httpd = server_class((HOST_NAME, PORT_NUMBER), MyHandler)
    httpd = ThreadedHTTPServer((HOST_NAME, PORT_NUMBER), MyHandler)
    print time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER)
