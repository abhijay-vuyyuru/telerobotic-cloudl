#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <string>
#include <fstream>
using namespace cv;
using namespace std;

 


 
std::vector<cv::Point2f> Generate2DPoints();
std::vector<cv::Point3f> Generate3DPoints();

 int main( int argc, char** argv )
 {

    VideoCapture cap("/home/abhijay/Desktop/OPENCV_on_Desktop/test.sdp");// Capture the network stream
    if ( !cap.isOpened() )  
    {
         cout << "Web cam cannot be opened" << endl;
         return -1;
    }
    

    ofstream out("out.txt"); // File to write the object coordinates to 
   

  int iLowH = 170;  //Declare variables to store HSV values
  int iHighH = 179;

  int iLowS = 150; 
  int iHighS = 255;

  int iLowV = 60;
  int iHighV = 255;


  int iLastX = -1; 
  int iLastY = -1;

  double posX;
  double posY;

  //Capture a temporary image from the camera
 Mat imgTmp;
 cap.read(imgTmp); 

  //Create a black image with the size as the camera output
 Mat imgLines = Mat::zeros( imgTmp.size(), CV_8UC3 );;
 

    while (true)
    {
        Mat imgOriginal;

        bool bSuccess = cap.read(imgOriginal); // read a new frame from video



         if (!bSuccess) //if not success, break loop
        {
             cout << "Cannot read a frame from video stream" << endl;
             break;
        }

    Mat imgHSV;

   cvtColor(imgOriginal, imgHSV, COLOR_BGR2HSV); //Convert the captured frame from BGR to HSV
 
  Mat imgThresholded;

   inRange(imgHSV, Scalar(iLowH, iLowS, iLowV), Scalar(iHighH, iHighS, iHighV), imgThresholded); //Threshold the image
      
  //morphological opening (removes small objects from the foreground)
  erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );
  dilate( imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) ); 

   //morphological closing (removes small holes from the foreground)
  dilate( imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) ); 
  erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );

   //Calculate the moments of the thresholded image
  Moments oMoments = moments(imgThresholded);

   double dM01 = oMoments.m01;
  double dM10 = oMoments.m10;
  double dArea = oMoments.m00;

   // if the area <= 10000, I consider that the there are no object in the image and it's because of the noise, the area is not zero 
  if (dArea > 10000)
  {
   //calculate the position of the ball
   posX = dM10 / dArea;
   posY = dM01 / dArea;
   printf("x = %f y=  %f\n", posX, posY);
   
   
   break;        
        
   if (iLastX >= 0 && iLastY >= 0 && posX >= 0 && posY >= 0)
   {
    //Draw a red line from the previous point to the current point
    line(imgLines, Point(posX, posY), Point(iLastX, iLastY), Scalar(0,0,255), 2);
   }

    iLastX = posX;
   iLastY = posY;
  }

   imshow("Thresholded Image", imgThresholded); //show the thresholded image

   imgOriginal = imgOriginal + imgLines;
  imshow("Original", imgOriginal); //show the original image

	 if (waitKey(30) == 27) //wait for 'esc' key press for 30ms. If 'esc' key is pressed, break loop
       {
            cout << "esc key is pressed by user" << endl;
            break; 
       }

}
	
	/*Code to perform matrix transformations*/

	std::vector<cv::Point2f> imagePoints = Generate2DPoints();
  std::vector<cv::Point3f> objectPoints = Generate3DPoints();
 
  std::cout << "There are " << imagePoints.size() << " imagePoints and " << objectPoints.size() << " objectPoints." << std::endl;
  cv::Mat cameraMatrix(3,3,cv::DataType<double>::type);
  //cv::setIdentity(cameraMatrix);
   cameraMatrix.at<double>(0) = 7.1209731781451023e+02;
   cameraMatrix.at<double>(1) = 0;
   cameraMatrix.at<double>(2) = 319.5;
   cameraMatrix.at<double>(3) = 0;
   cameraMatrix.at<double>(4) = 7.1209731781451023e+02;
   cameraMatrix.at<double>(5) = 239.5;
   cameraMatrix.at<double>(6) = 0;
   cameraMatrix.at<double>(7) = 0;
   cameraMatrix.at<double>(8) = 1;
 
  std::cout << "Initial cameraMatrix: " << cameraMatrix << std::endl;
 
  cv::Mat distCoeffs(1,5,cv::DataType<double>::type);
  distCoeffs.at<double>(0) = 1.6303168928416317e-01;
  distCoeffs.at<double>(1) = -3.8360679726230190e-01;
  distCoeffs.at<double>(2) = 0;
  distCoeffs.at<double>(3) = 0;
  distCoeffs.at<double>(4) =  1.7291692941164501e-02;
 
  cv::Mat rvec(3,1,cv::DataType<double>::type);
  cv::Mat tvec(3,1,cv::DataType<double>::type);
  cv::Mat rotationMatrix(3,3,cv::DataType<double>::type);
  cv::Mat rotationTranslationMatrix(3,4,cv::DataType<double>::type);
  cv::Mat projectionMatrix(3,4,cv::DataType<double>::type);
  cv::Mat imageCoordinates(3,1,cv::DataType<double>::type);
  cv::Mat worldCoordinates(4,1,cv::DataType<double>::type);
  cv::Mat col12R(3,2,cv::DataType<double>::type);
  cv::Mat poseMatrix(3,3,cv::DataType<double>::type);
  cv::Mat normalizedPoseMatrix(3,3,cv::DataType<double>::type);
  cv::Mat newProjectionMatrix(3,3,cv::DataType<double>::type);
  cv::Mat newProjectionMatrixInv(3,3,cv::DataType<double>::type);
  cv::Mat ivec(3,1,cv::DataType<double>::type);
  cv::Mat ovec(3,1,cv::DataType<double>::type);
  cv::Mat base(3,1,cv::DataType<double>::type);

  base.at<double>(0) = 418.64;
  base.at<double>(1) =   0;
  base.at<double>(2) = 587.52;
  


  ivec.at<double>(0) = posX;
  ivec.at<double>(1) =    posY;
  ivec.at<double>(2) = 1;
  

  cv::solvePnP(objectPoints, imagePoints, cameraMatrix, distCoeffs, rvec, tvec);
 
  std::cout << "rvec: " << rvec << std::endl;
  std::cout << "tvec: " << tvec << std::endl;
 
  std::vector<cv::Point2f> projectedPoints;
  cv::projectPoints(objectPoints, rvec, tvec, cameraMatrix, distCoeffs, projectedPoints);
 
  for(unsigned int i = 0; i < projectedPoints.size(); ++i)
    {
    std::cout << "Image point: " << imagePoints[i] << " Projected to " << projectedPoints[i] << std::endl;
    }
 
  
  cv::Rodrigues(rvec,rotationMatrix);
  std::cout << "Initial rotationMatrix: " << rotationMatrix << std::endl;

  col12R = rotationMatrix.colRange(0, 2);

 std::cout << "col12R: " << col12R << std::endl;

  cv::hconcat(rotationMatrix, tvec, rotationTranslationMatrix);
  cv::hconcat(col12R, tvec, poseMatrix);

  std::cout << "rotationTranslationMatrix: " << rotationTranslationMatrix << std::endl;
  std::cout << "POSE: " << poseMatrix << std::endl;

  projectionMatrix = cameraMatrix*rotationTranslationMatrix;
  newProjectionMatrix = cameraMatrix*poseMatrix  ;

  std::cout << "projectionMatrix: " << projectionMatrix << std::endl;
  std::cout << "NEW projectionMatrix: " << newProjectionMatrix << std::endl;
  double prev = 1/poseMatrix.at<double>(8);

  normalizedPoseMatrix = prev*poseMatrix;

  newProjectionMatrixInv = newProjectionMatrix.inv();
  std::cout << "Normalized poseMatrix: " << normalizedPoseMatrix << std::endl;
  std::cout << "Inverted projection matrix: " << newProjectionMatrixInv << std::endl;


/*Just some thing new    */
//double prev3 = 1/newProjectionMatrix.at<double>(8);
//newProjectionMatrix = prev3*newProjectionMatrix;
  
//Choose one of the methods
   
  //ovec = normalizedPoseMatrix*ivec;
  ovec = newProjectionMatrixInv*ivec;

  double prev2 = 1/ovec.at<double>(2); 
   std::cout << "Scaling Factor: " << prev2 << std::endl;

   ovec = prev2*ovec;

  std::cout << "Output Vector: " << ovec<< std::endl;

  ovec = ovec - base;

  std::cout << "Offset Vector: " << ovec<< std::endl;
  out << ovec.at<double>(0);
   out<<std::endl;
   out << ovec.at<double>(1);
   out.close();
 


	/*Matrix code added*/

       
    

   return 0;
}

std::vector<cv::Point2f> Generate2DPoints()
{
  std::vector<cv::Point2f> points;
 
  float x,y;
 
  x=6;y=223; //Done again
  points.push_back(cv::Point2f(x,y));
 
  x=623;y=265; //Done again
  points.push_back(cv::Point2f(x,y));
 
  x=490;y=269;//Done again
  points.push_back(cv::Point2f(x,y));
 
  x=128;y=246;// Done again
  points.push_back(cv::Point2f(x,y));
 
  x=257;y=236; //Done again
  points.push_back(cv::Point2f(x,y));
 
  x=360;y=240; //Done again
  points.push_back(cv::Point2f(x,y));
 
  //x=309;y=237;//Done again
  //points.push_back(cv::Point2f(x,y));

  x=44;y=214;//Done again
  points.push_back(cv::Point2f(x,y));

  
 
  for(unsigned int i = 0; i < points.size(); ++i)
    {
    std::cout << points[i] << std::endl;
    }
 
  return points;
}
 
 
std::vector<cv::Point3f> Generate3DPoints()
{
  std::vector<cv::Point3f> points;
 
 
  float x,y,z;

  x=438.6398;y=239.9993;z=0;//281.5852;
  points.push_back(cv::Point3f(x,y,z));
 
 
  x=438.6393;y=-240.0008;z=0;//276.1017;
  points.push_back(cv::Point3f(x,y,z));
 
  x=428.6405;y=-140.0002;z=0;//268.7736;
  points.push_back(cv::Point3f(x,y,z));
 
  x=428.6404;y=140;z=0;//271.0342;
  points.push_back(cv::Point3f(x,y,z));
 
  x=443.6404;y=39.99898;z=0;//274.438;
  points.push_back(cv::Point3f(x,y,z));
 
  x=443.6411;y=-39.9987;z=0;//272.9384;
  points.push_back(cv::Point3f(x,y,z));
 
  //x=443.6396;y=0.0006845812;z=0;//266.4672;
  //points.push_back(cv::Point3f(x,y,z));

 x=449.4404;y=202.1999;z=0;//266.4672;
  points.push_back(cv::Point3f(x,y,z));
 
  for(unsigned int i = 0; i < points.size(); ++i)
    {
    std::cout << points[i] << std::endl;
    }
 
  return points;
}
